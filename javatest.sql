-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 12 Sep 2021 pada 18.24
-- Versi Server: 5.5.32
-- Versi PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `javatest`
--
CREATE DATABASE IF NOT EXISTS `javatest` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `javatest`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_saving_account`
--

CREATE TABLE IF NOT EXISTS `t_saving_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `saving_tenor` int(11) NOT NULL,
  `first_deposit` bigint(20) NOT NULL,
  `monthly_deposit` bigint(20) NOT NULL,
  `purpose` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `t_saving_account`
--

INSERT INTO `t_saving_account` (`id`, `saving_tenor`, `first_deposit`, `monthly_deposit`, `purpose`) VALUES
(1, 3, 30000, 10000, 'EDU');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
