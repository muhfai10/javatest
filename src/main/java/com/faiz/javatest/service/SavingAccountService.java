package com.faiz.javatest.service;

import com.faiz.javatest.model.SavingAccount;
import com.faiz.javatest.repository.SavingAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SavingAccountService {
    @Autowired
    private SavingAccountRepository savingAccountRepository;

    public Optional<SavingAccount> findById(Long id) {
        return savingAccountRepository.findById(id);
    }

    public List<SavingAccount> findAll(){
        return savingAccountRepository.findAll();
    }

    public SavingAccount register(SavingAccount savingAccount) {
        return savingAccountRepository.save(savingAccount);
    }
}
