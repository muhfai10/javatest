package com.faiz.javatest.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "t_saving_account")
@Data
public class SavingAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "saving_tenor")
    private Long savingTenor;

    @Column(name = "first_deposit")
    private Long firstDeposit;

    @Column(name = "monthly_deposit")
    private Long monthlyDeposit;

    @Column(name = "purpose")
    private String purpose;
}
