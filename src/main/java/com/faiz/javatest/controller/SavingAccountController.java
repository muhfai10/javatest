package com.faiz.javatest.controller;

import com.faiz.javatest.model.SavingAccount;
import com.faiz.javatest.service.SavingAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/savingaccount")
public class SavingAccountController {
    @Autowired
    private SavingAccountService savingAccountService;

    @PostMapping(value = "register", consumes = "application/json")
    @ResponseBody
    public SavingAccount register(@RequestBody SavingAccount savingAccount){
        return savingAccountService.register(savingAccount);
    }

    @PostMapping(value = "calculate", consumes = "application/json")
    @ResponseBody
    public Double calculate(@RequestBody SavingAccount savingAccount){
       BigDecimal savingTenor = Objects.isNull(savingAccount.getSavingTenor()) ? new BigDecimal(0) : new BigDecimal(savingAccount.getSavingTenor());
       BigDecimal firstDepo = Objects.isNull(savingAccount.getFirstDeposit()) ? new BigDecimal(0) :new BigDecimal(savingAccount.getFirstDeposit());
       BigDecimal monthlyDepo = Objects.isNull(savingAccount.getMonthlyDeposit()) ? new BigDecimal(0) :new BigDecimal(savingAccount.getMonthlyDeposit());
       Double result = ((savingTenor.doubleValue()/12) * 6 / 100) * ((monthlyDepo.doubleValue()*savingTenor.doubleValue()) + firstDepo.doubleValue()) +   ((monthlyDepo.doubleValue()*savingTenor.doubleValue()) + firstDepo.doubleValue());
       return result;
    }

    @GetMapping(value = "findById/{id}")
    @ResponseBody
    public SavingAccount findById(@PathVariable("id") Long id){
        Optional<SavingAccount> optSaving = savingAccountService.findById(id);
        SavingAccount savingAccount = optSaving.get();
        return savingAccount;
    }

    @GetMapping(value = "findAll")
    @ResponseBody
    public List<SavingAccount> findAll(){
        return savingAccountService.findAll();
    }
}
